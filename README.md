# gadgetclub

## Project overview
*gadgetclub*は、*チップとデールの大作戦 レスキュー・レンジャーズ*に登場するキャラクタ、*ガジェット・ハックレンチ*を紹介するために制作しているウェブサイトです。  
すでにインターネット上にはこのキャラクターについてのWikiが存在するため、それに追従するつもりはありません。  
代わりに、ここではWikiには載っていないようなニッチで、誰もまとめていなさそうな情報を提供していきます。

_gadgetclub_ is a website created to introduce the character _Gadget Hackwrench_ from _Chip 'n Dale Rescue Rangers_.  
Since there is already a Wiki about this character available on the internet, I do not intend to follow it.  
Instead, here I will provide niche information that is not covered in the Wiki and seems to be overlooked by others.

## Website
You can visit the website [here](http://gadgetclub.starfree.jp).
