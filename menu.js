// menu.js
var currentPagePath = window.location.pathname;

function generateRelativePath(targetPage) {
	var pathSegments = currentPagePath.split('/');
	//Insert relative path when local
	var relativePath = "";

	for (var i = 0; i < pathSegments.length - 2; i++) {
		//change to "./" when necessary
		relativePath += "../";
	}

	relativePath += targetPage;

	return relativePath;
}

document.write('<div class="menu">\
<style>\
	.menu {\
		width: 300px;\
		height: 100%;\
		background-color: #E7BBDC;\
		float: left;\
		text-align: left;\
		border-radius: 10px;\
	}\
	div.menu h1 {\
		margin-left: 40px;\
	}\
	div.menu ul {\
		list-style: none;\
		font-size: 20px;\
	}\
	div.menu li {\
		margin: 10px 0;\
	}\
</style>\
<h1>メニュー</h1>\
	<ul>\
		<li><a href="' + generateRelativePath("index.html") + '">ホーム</a></li>\
		<li><a href="' + generateRelativePath("her/index.html") + '">ガジェットを知ろう</a></li>\
		<li><a href="' + generateRelativePath("inventions/index.html") + '">ガジェットの発明品</a></li>\
		<li><a href="' + generateRelativePath("fashions/index.html") + '">ガジェットのファッション</a></li>\
		<li><a href="' + generateRelativePath("episodes/index.html") + '">各エピソードのガジェット</a></li>\
		<li><a href="' + generateRelativePath("pronunciation/index.html") + '">発音</a></li>\
		<li><a href="' + generateRelativePath("misc/index.html") + '">その他</a></li>\
	</ul>\
</div>');
